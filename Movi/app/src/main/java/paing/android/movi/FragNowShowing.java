package paing.android.movi;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import paing.android.movi.model.MoviProviderContract;
import paing.android.movi.model.Movie;
import paing.android.movi.utils.Constants;
import paing.android.movi.utils.LogUtil;
import paing.android.movi.utils.NowShowingAdapter;
import paing.android.movi.utils.Util;
import paing.android.movi.views.EndlessGridView;

/**
 * Created by paing on 15/1/16.
 */
public class FragNowShowing extends Fragment implements EndlessGridView.EndlessListener, LoaderManager.LoaderCallbacks<Cursor> {
    NowShowingAdapter adapter;
    EndlessGridView grid;
    SwipeRefreshLayout refreshLayout;
    ProgressBar progressBar;
    Snackbar errorMessageSnackBar;

    //favourite operation may overlap with data refresh. Better be safe with thread-safe arraylist
    private CopyOnWriteArrayList<Long> favouriteMovieIds = new CopyOnWriteArrayList<>();

    int startPage = 1;
    int currentPage = 1;
    int totalPage = -1;
    private boolean isLoading = false;

    private static final int MSG_LOAD_DATA = 100;
    private MyHandler myHandler = new MyHandler(this);

    static class MyHandler extends Handler {
        private final WeakReference<FragNowShowing> mFrag;

        MyHandler(FragNowShowing fragment) {
            mFrag = new WeakReference<FragNowShowing>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            FragNowShowing fragment = mFrag.get();
            if (fragment == null || fragment.getActivity() == null) {
                return;
            }
            switch (msg.what) {
                case MSG_LOAD_DATA:
                    fragment.loadData();
                    break;
            }
        }
    }

    public static FragNowShowing newInstance() {
        FragNowShowing fragment = new FragNowShowing();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInsanceState) {
        super.onCreate(savedInsanceState);
        startPage = 1;
        currentPage = startPage - 1;
        totalPage = -1;
        myHandler.sendEmptyMessageDelayed(MSG_LOAD_DATA, 500);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_now_showing, container, false);
        grid = (EndlessGridView) rootView.findViewById(R.id.grid);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress);
        refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);

        //calculate available space for grid's num columns
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        int oneItemWidth = getResources().getDimensionPixelSize(R.dimen.cover_image_width);
        int numColumns = screenWidth / oneItemWidth;
        int gap = (screenWidth - (numColumns * oneItemWidth)) / (numColumns - 1);
        grid.setVerticalSpacing(gap);
        grid.setNumColumns(numColumns);
        grid.setLoadingView(R.layout.grid_loading_view);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = startPage - 1;
                myHandler.sendEmptyMessage(MSG_LOAD_DATA);
            }
        });

        adapter = new NowShowingAdapter(getActivity(), new ArrayList<Movie>());
        grid.setAdapter(adapter);
        grid.setListener(this);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movie movie = adapter.getItem(position);
                if (movie != null) {
                    Intent details = new Intent(getActivity(), DetailsActivity.class);
                    Gson gson = new Gson();
                    details.putExtra(DetailsActivity.EXTRA_MOVIE, gson.toJson(movie));
                    startActivity(details);
                }
            }
        });

        getLoaderManager().initLoader(0, null, this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            grid.setNestedScrollingEnabled(true);
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(fabClickReceiver, new IntentFilter(MainActivity.ACTION_FAB_CLICKED));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(fabClickReceiver);
    }

    @Override
    public void setUserVisibleHint(boolean visibleHint) {
        super.setUserVisibleHint(visibleHint);
        if (adapter != null)
            adapter.setFavourites(favouriteMovieIds);
    }

    private void dismissLoading() {
        progressBar.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void loadData() {
        if (adapter.getCount() == 0)
            progressBar.setVisibility(View.VISIBLE);
        new DataTask(this).execute(currentPage + 1);
    }

    private void showErrorMessage(String message, String actionText, final Runnable retryAction) {
        if (errorMessageSnackBar != null)
            errorMessageSnackBar.dismiss();

        errorMessageSnackBar = Snackbar.make(getView(), message, Snackbar.LENGTH_INDEFINITE);
        errorMessageSnackBar.show();
        errorMessageSnackBar.setAction(actionText, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorMessageSnackBar.dismiss();
                if (retryAction != null)
                    retryAction.run();
            }
        });
    }

    public static class DataTask extends AsyncTask<Integer, Integer, ArrayList<Movie>> {
        WeakReference<FragNowShowing> fragment;
        String errorMessage = null;

        DataTask(FragNowShowing frag) {
            fragment = new WeakReference<FragNowShowing>(frag);
        }

        protected ArrayList<Movie> doInBackground(Integer... args) {
            if (fragment.get() == null)
                return null;
            if (!Util.isNetworkAvailable(fragment.get().getContext())) {
                errorMessage = fragment.get().getString(R.string.network_not_available);
                return null;
            }
            try {
                JsonObject result = Ion.with(fragment.get().getContext())
                        .load("GET", Constants.addParamsToURL(Constants.URL_GET_NOW_SHOWING, Constants.PARAM_PAGE, String.valueOf(args[0])))
                        .asJsonObject().get();
                if (fragment.get() == null)
                    return null;

                LogUtil.i("result", result.toString());
                int resultPageNo = result.get(Constants.KEY_PAGE).getAsInt();
                int totalPageNo = result.get(Constants.KEY_TOTAL_PAGES).getAsInt();
                publishProgress(resultPageNo, totalPageNo);
                JsonArray data = result.get(Constants.KEY_RESULTS).getAsJsonArray();
                Gson gson = new GsonBuilder().create();
                ArrayList<Movie> movies = new ArrayList<>();
                Movie movie = null;
                for (JsonElement element : data) {
                    movie = gson.fromJson(element, Movie.class);
                    movie.set_id(movie.getId());
                    movie.setGenre_ids_string(element.getAsJsonObject().get(Constants.KEY_GENRE_IDS).toString());
                    movie.setFavourite(fragment.get().favouriteMovieIds.contains(movie.getId()));
                    movies.add(movie);
                }
                return movies;
            } catch (Exception e) {
                errorMessage = e.getLocalizedMessage();
                e.printStackTrace();
                return null;
            }
        }

        protected void onProgressUpdate(Integer... progress) {
            if (fragment.get() == null)
                return;
            FragNowShowing frag = fragment.get();
            frag.currentPage = progress[0];
            frag.totalPage = progress[1];
        }

        protected void onPostExecute(ArrayList<Movie> result) {
            if (fragment.get() == null)
                return;
            final FragNowShowing frag = fragment.get();
            frag.dismissLoading();
            if (result == null) {
                frag.showErrorMessage(errorMessage, frag.getString(R.string.retry), new Runnable() {
                    @Override
                    public void run() {
                        frag.loadData();
                    }
                });
            } else {
                frag.grid.addNewData(result);
            }
        }
    }

    private BroadcastReceiver fabClickReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (grid != null && getUserVisibleHint())
                grid.smoothScrollToPosition(0);
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), MoviProviderContract.MOVIE_URI,
                new String[]{MoviProviderContract.COLUMN__ID}, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        favouriteMovieIds.clear();
        for (data.moveToFirst(); !data.isAfterLast(); data.moveToNext()) {
            favouriteMovieIds.add(data.getLong(0)); //0 is definite since we only query one column
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        favouriteMovieIds.clear();
    }
}
