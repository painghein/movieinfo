package paing.android.movi.utils;

public class MoviException extends Exception{

	private static final long serialVersionUID = 1L;

    private final int exceptionCode;
    public MoviException(int code, String message) {
        super(message);
        this.exceptionCode = code;
    }

	public MoviException(String message) {
        this(-1,message);
    }

    public int getExceptionCode() {
        return exceptionCode;
    }
}
