package paing.android.movi.model;

import android.net.Uri;

public final class MoviProviderContract {
    public static String AUTHORITY = "paing.android.movi";
    public static final Uri MOVIE_URI = Uri.parse("content://" + AUTHORITY + "/movie");
    public static final Uri GENRE_URI = Uri.parse("content://" + AUTHORITY + "/genre");

    public static final String COLUMN__ID = "_id";
    public static final String COLUMN_SORT_ORDER = "sortOrder";
    public static final String COLUMN_PAGE_NO = "pageNo";
    public static final String COLUMN_DATE_ADDED = "dateAdded";

}