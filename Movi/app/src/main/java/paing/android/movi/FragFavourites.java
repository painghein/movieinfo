package paing.android.movi;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.test.mock.MockDialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import paing.android.movi.model.MoviProviderContract;
import paing.android.movi.model.Movie;
import paing.android.movi.utils.Constants;
import paing.android.movi.utils.FavouriteAdapter;
import paing.android.movi.utils.LogUtil;
import paing.android.movi.views.EndlessGridView;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by paing on 15/1/16.
 */
public class FragFavourites extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    FavouriteAdapter adapter;
    GridView grid;


    public static FragFavourites newInstance() {
        FragFavourites fragment = new FragFavourites();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInsanceState) {
        super.onCreate(savedInsanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favourite, container, false);
        grid = (GridView) rootView.findViewById(R.id.grid);


        //calculate available space for grid's num columns
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        int oneItemWidth = getResources().getDimensionPixelSize(R.dimen.cover_image_width);
        int numColumns = screenWidth / oneItemWidth;
        int gap = (screenWidth - (numColumns * oneItemWidth)) / (numColumns - 1);
        grid.setVerticalSpacing(gap);
        grid.setNumColumns(numColumns);

        adapter = new FavouriteAdapter(getActivity(), null);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = adapter.getCursor();
                cursor.moveToPosition(position);
                Movie movie = cupboard().withCursor(cursor).get(Movie.class);
                if (movie != null) {
                    Intent details = new Intent(getActivity(), DetailsActivity.class);
                    Gson gson = new Gson();
                    details.putExtra(DetailsActivity.EXTRA_MOVIE, gson.toJson(movie));
                    startActivity(details);
                }
            }
        });


        getLoaderManager().initLoader(0, null, this);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            grid.setNestedScrollingEnabled(true);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(fabClickReceiver, new IntentFilter(MainActivity.ACTION_FAB_CLICKED));
    }

    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(fabClickReceiver);
    }

    private void dismissLoading() {
    }


    private BroadcastReceiver fabClickReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (grid != null && getUserVisibleHint())
                grid.smoothScrollToPosition(0);
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), MoviProviderContract.MOVIE_URI,
                null, null, null,
                MoviProviderContract.COLUMN_DATE_ADDED + " DESC");

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        dismissLoading();
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}
