package paing.android.movi.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by paing on 14/1/16.
 */
public class Constants {
    //internal message related
    public static final String KEY_RESULT = "result";
    public static final String KEY_MESSAGE = "message";

    public static final int RESULT_FAILURE = 0;
    public static final int RESULT_SUCCESS = 1;


    //API configs
    public static final String PARAM_API_KEY = "api_key";
    public static final String API_KEY = "b34a81e46da5200c6abe8adb9df0ced5";

    private static final String URL_HOST = "http://api.themoviedb.org/3";

    public static final String URL_GET_GENRE = URL_HOST + "/genre/movie/list?" + PARAM_API_KEY + "=" + API_KEY;
    public static final String URL_GET_NOW_SHOWING = URL_HOST + "/movie/now_playing?" + PARAM_API_KEY + "=" + API_KEY;
    private static final String URL_GET_VIDEOS = URL_HOST + "/movie/%s/videos?" + PARAM_API_KEY + "=" + API_KEY;
    private static final String URL_GET_SIMILAR = URL_HOST + "/movie/%s/similar?" + PARAM_API_KEY + "=" + API_KEY;
    private static final String URL_YOUTUBE_IMG = "https://img.youtube.com/vi/%s/0.jpg";
    public static final String PARAM_PAGE = "page";

    public static String getVideosURL(String movieId) {
        return String.format(URL_GET_VIDEOS, movieId);
    }

    public static String getSimilarURL(String movieId) {
        return String.format(URL_GET_SIMILAR, movieId);
    }

    public static String getYoutubeImageURL(String key) {
        return String.format(URL_YOUTUBE_IMG, key);
    }

    public static String getCoverImageURL(String path) {
        return "http://image.tmdb.org/t/p/w500/" + path + "?" + PARAM_API_KEY + "=" + API_KEY;
    }

    /**
     * Append param to URL
     *
     * @param url   Original URL. Assume that this already has an API key parameter
     * @param key
     * @param value Value to add to url. This value will be URL encoded
     * @return url string with appended key and value pair
     * @throws UnsupportedEncodingException If value contains no-encodeable characters
     */
    public static String addParamsToURL(String url, String key, String value) throws UnsupportedEncodingException {
        return url + "&" + key + "=" + URLEncoder.encode(value, "utf-8");
    }

    //API Return data keys
    public static final String KEY_PAGE = "page";
    public static final String KEY_RESULTS = "results";
    public static final String KEY_GENRE_IDS = "genre_ids";
    public static final String KEY_TOTAL_PAGES = "total_pages";
    public static final String KEY_TOTAL_RESULTS = "total_results";

}
