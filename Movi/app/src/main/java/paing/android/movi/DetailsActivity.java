package paing.android.movi;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ArrayRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import paing.android.movi.model.MoviProviderContract;
import paing.android.movi.model.Movie;
import paing.android.movi.utils.Constants;
import paing.android.movi.utils.InternalStorageContentProvider;
import paing.android.movi.utils.LogUtil;
import paing.android.movi.utils.RelatedMovieAdapter;
import paing.android.movi.utils.Util;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class DetailsActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {
    public static final String EXTRA_MOVIE = "paing.android.EXTRA_MOVIE";

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private int mMaxScrollSize;

    ImageView imgCover;
    private AppCompatCheckBox checkBox;
    private RecyclerView recyclerView;
    private RelatedMovieAdapter recyclerAdapter;

    Movie movie;
    Snackbar errorMessageSnackBar;
    ArrayList<Movie> relatedMovies;
    ProgressBar progress;
    View emptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_content);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView tvTitle = (TextView) findViewById(R.id.lbl_title);
        TextView tvSecondaryTitle = (TextView) findViewById(R.id.lbl_secondary_title);
        ImageView imgBackDrop = (ImageView) findViewById(R.id.img_backdrop);
        imgCover = (ImageView) findViewById(R.id.img_cover);
        TextView lblOverView = (TextView) findViewById(R.id.lbl_overview);
        checkBox = (AppCompatCheckBox) findViewById(R.id.btn_bookmarks);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        emptyView = findViewById(R.id.empty_view);
        progress = (ProgressBar) findViewById(R.id.progress);

        //action listeners
        checkBox.setOnClickListener(this);
        findViewById(R.id.btn_action_1).setOnClickListener(this);
        findViewById(R.id.btn_share).setOnClickListener(this);

        toolbar.setTitle(R.string.app_name);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        appBarLayout.addOnOffsetChangedListener(this);

        Gson gson = new GsonBuilder().create();
        String extraString = getIntent().getStringExtra(EXTRA_MOVIE);
        if (extraString != null) {
            movie = gson.fromJson(extraString, Movie.class);
        }

        //set data
        if (movie != null) {
            tvTitle.setText(movie.getTitle());
            lblOverView.setText(movie.getOverview());
            checkBox.setChecked(movie.isFavourite());
            Picasso.with(this).load(movie.getBackDropUrl())
                    .placeholder(R.drawable.material_flat)
                    .error(R.drawable.material_flat)
                    .into(imgBackDrop);
            Picasso.with(this).load(movie.getPosterUrl()).
                    resize(getResources().getDimensionPixelSize(R.dimen.cover_image_width), getResources().getDimensionPixelSize(R.dimen.cover_image_height))
                    .placeholder(R.drawable.ic_launcher)
                    .error(R.drawable.ic_error)
                    .centerCrop().into(imgCover);

            //related movie section
            relatedMovies = new ArrayList<>();
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            recyclerAdapter = new RelatedMovieAdapter(this, relatedMovies);
            recyclerView.setAdapter(recyclerAdapter);

            loadRelatedMovies();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (movie == null)
            finish();
    }


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

        if (percentage == 100) {
            toolbar.setTitle(movie != null ? movie.getTitle() : "");
        } else {
            toolbar.setTitle("");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_action_1:
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, getString(R.string.dummy_action_text), Snackbar.LENGTH_SHORT);
                snackbar.show();
                break;
            case R.id.btn_share:
                shareMovie();
                break;
            case R.id.btn_bookmarks:
                movie.setFavourite(checkBox.isChecked());
                if (checkBox.isChecked()) {
                    //add to db
                    movie.setDateAdded(new Date().getTime());
                    getContentResolver().insert(MoviProviderContract.MOVIE_URI, cupboard().withEntity(Movie.class).toContentValues(movie));
                } else {
                    //remove from db
                    getContentResolver().delete(ContentUris.withAppendedId(MoviProviderContract.MOVIE_URI, movie.getId()), null, null);
                }
                break;
        }
    }

    /**
     * Share movie to other apps.
     * Share intent includes image and title
     */
    private void shareMovie() {
        if (movie == null)
            return;
        try {
            Bitmap bitmap = ((BitmapDrawable) imgCover.getDrawable()).getBitmap();
            FileOutputStream outputStream = new FileOutputStream(new File(getFilesDir(),
                    InternalStorageContentProvider.TEMP_PHOTO_FILE_NAME));
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
            outputStream.close();

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, movie.getTitle());
            shareIntent.putExtra(Intent.EXTRA_STREAM, InternalStorageContentProvider.CONTENT_URI);
            shareIntent.setType("image/*");
            startActivity(Intent.createChooser(shareIntent, getString(R.string.share_movie)));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadRelatedMovies() {
        progress.setVisibility(View.VISIBLE);
        relatedMovies.clear();
        new DataTask(this).execute(String.valueOf(movie.getId()));
    }

    private void dismissProgress() {
        progress.setVisibility(View.GONE);
    }

    private void showErrorMessage(String message, String actionText, final Runnable retryAction) {
        if (errorMessageSnackBar != null)
            errorMessageSnackBar.dismiss();

        errorMessageSnackBar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_INDEFINITE);
        errorMessageSnackBar.show();
        errorMessageSnackBar.setAction(actionText, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorMessageSnackBar.dismiss();
                if (retryAction != null)
                    retryAction.run();
            }
        });
    }

    public static class DataTask extends AsyncTask<String, Integer, ArrayList<Movie>> {
        WeakReference<DetailsActivity> activityRef;
        String errorMessage = null;

        DataTask(DetailsActivity activity) {
            activityRef = new WeakReference<DetailsActivity>(activity);
        }

        protected ArrayList<Movie> doInBackground(String... args) {
            if (activityRef.get() == null)
                return null;

            if (!Util.isNetworkAvailable(activityRef.get())) {
                errorMessage = activityRef.get().getString(R.string.network_not_available);
                return null;
            }

            try {
                JsonObject result = Ion.with(activityRef.get())
                        .load("GET", Constants.getSimilarURL(args[0]))
                        .asJsonObject().get();
                if (activityRef.get() == null)
                    return null;

                Cursor cursor = activityRef.get().getContentResolver().query(MoviProviderContract.MOVIE_URI,
                        new String[]{MoviProviderContract.COLUMN__ID}, null, null, null);
                List<Long> favouriteIds = new ArrayList<>();
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    favouriteIds.add(cursor.getLong(0)); //0 is definite since we only query one column
                }

                LogUtil.i("result", result.toString());
                int resultPageNo = result.get(Constants.KEY_PAGE).getAsInt();
                int totalPageNo = result.get(Constants.KEY_TOTAL_PAGES).getAsInt();
                publishProgress(resultPageNo, totalPageNo);
                JsonArray data = result.get(Constants.KEY_RESULTS).getAsJsonArray();
                Gson gson = new GsonBuilder().create();
                ArrayList<Movie> movies = new ArrayList<>();
                Movie movie = null;
                for (JsonElement element : data) {
                    movie = gson.fromJson(element, Movie.class);
                    movie.set_id(movie.getId());
                    movie.setGenre_ids_string(element.getAsJsonObject().get(Constants.KEY_GENRE_IDS).toString());
                    movie.setFavourite(favouriteIds.contains(movie.getId()));
                    movies.add(movie);
                }
                return movies;
            } catch (Exception e) {
                errorMessage = e.getLocalizedMessage();
                e.printStackTrace();
                return null;
            }
        }

        protected void onProgressUpdate(Integer... progress) {
            if (activityRef.get() == null)
                return;
        }

        protected void onPostExecute(ArrayList<Movie> result) {
            if (activityRef.get() == null)
                return;
            final DetailsActivity activity = activityRef.get();
            activity.dismissProgress();
            if (result == null) {
                activity.showErrorMessage(errorMessage, activity.getString(R.string.retry), new Runnable() {
                    @Override
                    public void run() {
                        activity.loadRelatedMovies();
                    }
                });
                return;
            }
            if (result.size() > 0) {
                activity.recyclerView.setVisibility(View.VISIBLE);
                activity.emptyView.setVisibility(View.GONE);
                activity.relatedMovies.addAll(result);
                activity.recyclerAdapter.notifyDataSetChanged();
            } else {
                activity.recyclerView.setVisibility(View.GONE);
                activity.emptyView.setVisibility(View.VISIBLE);
            }
        }
    }
}
