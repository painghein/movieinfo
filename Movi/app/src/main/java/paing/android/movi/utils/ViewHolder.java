package paing.android.movi.utils;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by paing on 17/1/16.
 */
public class ViewHolder extends RecyclerView.ViewHolder {
    ImageView imgCover;
    AppCompatCheckBox checkBox;
    public ViewHolder(View itemView) {
        super(itemView);
    }
}
