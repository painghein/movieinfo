package paing.android.movi.utils;

import android.app.ActionBar;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import java.util.Date;

import paing.android.movi.R;
import paing.android.movi.model.MoviProviderContract;
import paing.android.movi.model.Movie;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by paing on 15/1/16.
 */
public class FavouriteAdapter extends CursorAdapter {

    Context context;
    int imgWidth, imgHeight;

    ContentResolver resolver;

    public FavouriteAdapter(Context context, Cursor c) {
        super(context, c, 0);
        this.context = context;

        imgWidth = context.getResources().getDimensionPixelSize(R.dimen.cover_image_width);
        imgHeight = context.getResources().getDimensionPixelSize(R.dimen.cover_image_height);

        resolver = context.getContentResolver();
    }


    @Override
    public View newView(Context context, final Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.grid_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        holder.imgCover = (ImageView) view.findViewById(R.id.image_cover);
        holder.checkBox = (AppCompatCheckBox) view.findViewById(R.id.check_box);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        final Movie movie = cupboard().withCursor(cursor).get(Movie.class);
        Picasso.with(context).load(movie.getPosterUrl()).
                resize(imgWidth, imgHeight)
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_error)
                .centerCrop().into(holder.imgCover);
        holder.checkBox.setOnCheckedChangeListener(null);
        holder.checkBox.setChecked(movie.isFavourite());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                movie.setFavourite(isChecked);
                if (isChecked) {
                    //add to db
                    movie.setDateAdded(new Date().getTime());
                    resolver.insert(MoviProviderContract.MOVIE_URI, cupboard().withEntity(Movie.class).toContentValues(movie));
                } else {
                    //remove from db
                    resolver.delete(ContentUris.withAppendedId(MoviProviderContract.MOVIE_URI, movie.getId()), null, null);
                }
            }
        });
    }
}
