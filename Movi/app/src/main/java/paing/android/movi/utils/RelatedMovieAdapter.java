package paing.android.movi.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import paing.android.movi.DetailsActivity;
import paing.android.movi.R;
import paing.android.movi.model.MoviProviderContract;
import paing.android.movi.model.Movie;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by paing on 17/1/16.
 */
public class RelatedMovieAdapter extends RecyclerView.Adapter<ViewHolder> {


    private List<Movie> data;
    private Context context;
    int imgWidth, imgHeight;
    private ContentResolver resolver;

    public RelatedMovieAdapter(Context context, ArrayList<Movie> data) {
        this.context = context;
        this.data = data;
        imgWidth = context.getResources().getDimensionPixelSize(R.dimen.cover_image_width);
        imgHeight = context.getResources().getDimensionPixelSize(R.dimen.cover_image_height);
        resolver = context.getContentResolver();
    }

    @Override
    public int getItemCount() {
        return (null != data ? data.size() : 0);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Movie movie = data.get(position);
        Picasso.with(context).load(movie.getPosterUrl()).
                resize(imgWidth, imgHeight)
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_error)
                .centerCrop().into(holder.imgCover);
        holder.checkBox.setOnCheckedChangeListener(null);
        holder.checkBox.setChecked(movie.isFavourite());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                movie.setFavourite(isChecked);
                if (isChecked) {
                    //add to db
                    movie.setDateAdded(new Date().getTime());
                    resolver.insert(MoviProviderContract.MOVIE_URI, cupboard().withEntity(Movie.class).toContentValues(movie));
                } else {
                    //remove from db
                    resolver.delete(ContentUris.withAppendedId(MoviProviderContract.MOVIE_URI, movie.getId()), null, null);
                }
            }
        });

        holder.imgCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent details = new Intent(context, DetailsActivity.class);
                Gson gson = new Gson();
                details.putExtra(DetailsActivity.EXTRA_MOVIE, gson.toJson(movie));
                context.startActivity(details);
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());
        ViewGroup rootView = (ViewGroup) mInflater.inflate(R.layout.grid_item, viewGroup, false);
        ViewHolder holder = new ViewHolder(rootView);
        holder.imgCover = (ImageView) rootView.findViewById(R.id.image_cover);
        holder.checkBox = (AppCompatCheckBox) rootView.findViewById(R.id.check_box);
        holder.imgCover.setClickable(true);
        return holder;
    }
}
