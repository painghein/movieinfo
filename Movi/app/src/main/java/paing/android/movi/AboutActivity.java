package paing.android.movi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;

/**
 * Created by paing on 17/1/16.
 */
public class AboutActivity extends AppCompatActivity {
    WebView wv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        wv = (WebView) findViewById(R.id.webView);
    }

    @Override
    public void onResume() {
        super.onResume();
        wv.postDelayed(new Runnable() {
            @Override
            public void run() {
                wv.loadUrl("file:///android_asset/android_open_source_software_notices.html");
            }
        }, 100);
    }
}
