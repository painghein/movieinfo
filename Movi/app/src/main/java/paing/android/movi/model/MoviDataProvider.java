package paing.android.movi.model;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import static paing.android.movi.model.MoviProviderContract.AUTHORITY;
import static paing.android.movi.model.MoviProviderContract.MOVIE_URI;
import static paing.android.movi.model.MoviProviderContract.GENRE_URI;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class MoviDataProvider extends ContentProvider {
    private MySQLiteHelper mDatabaseHelper;
    private static UriMatcher sMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private static final int MOVIE = 0;
    private static final int MOVIES = 1;

    private static final int GENRE = 2;
    private static final int GENRES = 3;


    static {
        sMatcher.addURI(AUTHORITY, "movie/#", MOVIE);
        sMatcher.addURI(AUTHORITY, "movie", MOVIES);

        sMatcher.addURI(AUTHORITY, "genre/#", GENRE);
        sMatcher.addURI(AUTHORITY, "genre", GENRES);
    }

    @Override
    public int delete(Uri uri, String selection, String[] args) {
        SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        int count = 0;
        switch (sMatcher.match(uri)) {
            case MOVIES:
                count = cupboard().withDatabase(db).delete(Movie.class, selection, args);
                break;
            case MOVIE:
                count = cupboard().withDatabase(db).delete(Movie.class, ContentUris.parseId(uri)) ? 1 : 0;
                break;
            case GENRES:
                count = cupboard().withDatabase(db).delete(Genre.class, selection, args);
                break;
            case GENRE:
                count = cupboard().withDatabase(db).delete(Genre.class, ContentUris.parseId(uri)) ? 1 : 0;
                break;
            default:
                throw new RuntimeException("Not supported URI");
        }
        if (count > 0)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        Uri newUri = null;
        switch (sMatcher.match(uri)) {
            case MOVIES:
            case MOVIE:
                newUri = Uri.parse(MOVIE_URI + "/" + cupboard().withDatabase(db).put(Movie.class, values));
                break;
            case GENRES:
            case GENRE:
                newUri = Uri.parse(GENRE_URI + "/" + cupboard().withDatabase(db).put(Genre.class, values));
                break;
            default:
                throw new RuntimeException("Not supported URI");
        }
        if (newUri != null)
            getContext().getContentResolver().notifyChange(uri, null);
        return newUri;
    }

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new MySQLiteHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        Cursor cursor = null;
        switch (sMatcher.match(uri)) {
            case MOVIES:
                cursor = cupboard().withDatabase(db).query(Movie.class)
                        .withProjection(projection)
                        .withSelection(selection, selectionArgs).orderBy(sortOrder)
                        .getCursor();
                break;
            case MOVIE:
                cursor = cupboard().withDatabase(db).query(Movie.class)
                        .byId(ContentUris.parseId(uri)).getCursor();
                break;
            case GENRES:
                cursor = cupboard().withDatabase(db).query(Genre.class)
                        .withProjection(projection)
                        .withSelection(selection, selectionArgs).orderBy(sortOrder)
                        .getCursor();
                break;
            case GENRE:
                cursor = cupboard().withDatabase(db).query(Genre.class)
                        .byId(ContentUris.parseId(uri)).getCursor();
                break;
            default:
                throw new RuntimeException("Not supported URI");
        }
        if (cursor != null)
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        int count = 0;
        switch (sMatcher.match(uri)) {
            case MOVIES:
            case MOVIE:
                count = cupboard().withDatabase(db).update(Movie.class, values, selection, selectionArgs);
                break;
            case GENRES:
            case GENRE:
                count = cupboard().withDatabase(db).update(Genre.class, values, selection, selectionArgs);
                break;
            default:
                throw new RuntimeException("Not supported URI");
        }
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }
}