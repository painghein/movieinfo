package paing.android.movi.model;

/**
 * Created by paing on 15/1/16.
 */
public class Genre {
    private Long _id;
    private long id;
    private String name;

    public Genre() {
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


