package paing.android.movi.utils;

import android.util.Log;

public class LogUtil {
    public static boolean DEBUG = true;

    public static void d(String tag, String msg) {
        if (DEBUG)
            try {
                Log.d(tag, msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public static void e(String tag, String msg) {
        if (DEBUG)
            try {
                Log.e(tag, msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public static void i(String tag, String msg) {
        if (DEBUG)
            try {
                Log.i(tag, msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public static void e(String tag, String errorMessage, Exception ioException) {
        if (DEBUG)
            try {
                Log.e(tag, errorMessage, ioException);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}
