package paing.android.movi.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import paing.android.movi.R;
import paing.android.movi.model.MoviProviderContract;
import paing.android.movi.model.Movie;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by paing on 15/1/16.
 */
public class NowShowingAdapter extends ArrayAdapter<Movie> {

    Context context;
    ArrayList<Movie> movies;
    int imgWidth, imgHeight;
    private ContentResolver resolver;

    public NowShowingAdapter(Context context, List<Movie> items) {
        super(context, 0);
        this.context = context;
        movies = new ArrayList<>();
        movies.addAll(items);
        imgWidth = context.getResources().getDimensionPixelSize(R.dimen.cover_image_width);
        imgHeight = context.getResources().getDimensionPixelSize(R.dimen.cover_image_height);
        resolver = context.getContentResolver();
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Movie getItem(int position) {
        return movies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null || convertView.getTag() == null) {
            convertView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                    inflate(R.layout.grid_item, parent, false);
            holder = new ViewHolder(convertView);
            holder.imgCover = (ImageView) convertView.findViewById(R.id.image_cover);
            holder.checkBox = (AppCompatCheckBox) convertView.findViewById(R.id.check_box);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Movie movie = movies.get(position);
        Picasso.with(context).load(movie.getPosterUrl()).
                resize(imgWidth, imgHeight)
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_error)
                .centerCrop().into(holder.imgCover);
        holder.checkBox.setOnCheckedChangeListener(null);
        holder.checkBox.setChecked(movie.isFavourite());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                movie.setFavourite(isChecked);
                if (isChecked) {
                    //add to db
                    movie.setDateAdded(new Date().getTime());
                    resolver.insert(MoviProviderContract.MOVIE_URI, cupboard().withEntity(Movie.class).toContentValues(movie));
                } else {
                    //remove from db
                    resolver.delete(ContentUris.withAppendedId(MoviProviderContract.MOVIE_URI, movie.getId()), null, null);
                }
            }
        });
        return convertView;
    }

    @Override
    public void addAll(Collection data) {
        movies.addAll(data);
    }

    /**
     * Sync favourites Id to data source.
     * This is needed since FragNowShowing does not keep the data.
     * All data are kept under adapter.
     *
     * @param favouritesIds
     */
    public void setFavourites(List<Long> favouritesIds) {
        for(Movie movie : movies){
            movie.setFavourite(favouritesIds.contains(movie.getId()));
        }
        notifyDataSetChanged();
    }

}
